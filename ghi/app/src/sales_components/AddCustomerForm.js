import React, {useState, useEffect} from 'react';

function AddCustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: ''
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            });
        };
    };

    const handleAddCustomerFormChange = (e) => {

        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form onSubmit={handleSubmit} id="create-customer-form">
                  <h1 className="card-title">Add a Customer</h1>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddCustomerFormChange} value={formData.first_name} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" />
                        <label htmlFor="first_name">First Name</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddCustomerFormChange} value={formData.last_name} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" />
                        <label htmlFor="last_name">Last Name</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddCustomerFormChange} value={formData.address} required placeholder="Address" type="text" id="address" name="address" className="form-control" />
                        <label htmlFor="address">Address</label>
                        </div>
                      </div>
                      <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddCustomerFormChange} value={formData.phone_number} required placeholder="Phone Number" type="text" id="phone_number" name="phone_number" className="form-control" />
                        <label htmlFor="phone_number">Phone Number</label>
                        </div>
                      </div>
                    </div>
                  <button className="btn btn-lg btn-primary">Add</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default AddCustomerForm;
