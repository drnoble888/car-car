import React, {useState, useEffect} from 'react';

function AddSaleForm() {
    const [autos, setAutos] = useState([]);
    const [salesperson, setSalesperson] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [sales, setSales] = useState([]);
    const [formData, setFormData] = useState({
       automobile: '',
       salesperson: '',
       customer: '',
       price: '',
       sold: '',
    });

    const getData = async () => {
        const autosUrl = `http://localhost:8100/api/automobiles/`;
        const autosResponse =  await fetch(autosUrl);

        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const salespersonResponse = await fetch(salespersonUrl);

        const customerUrl = 'http://localhost:8090/api/customers/';
        const customerResponse = await fetch(customerUrl);

        const salesUrl = 'http://localhost:8090/api/sales/';
        const salesResponse = await fetch(salesUrl);

        if (autosResponse.ok && salespersonResponse.ok && customerResponse.ok && salesResponse.ok) {
            const autosData = await autosResponse.json();
            const salespersonData = await salespersonResponse.json();
            const customerData = await customerResponse.json();
            const salesData = await salesResponse.json();

            setAutos(autosData.autos);
            setSalesperson(salespersonData.salespeople);
            setCustomers(customerData.customers);
            setSales(salesData.sales);
        }
      };

    useEffect(() => {
        getData();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = `http://localhost:8090/api/sales/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        if (response.ok) {
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
        }
        getData();
    };

    const handleSaleFormChange = (e) => {

        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const saleVins = [];
    for(let sale of sales) {
      saleVins.push(sale.automobile.vin)
    };

    const notSold = [];
    for(let auto of autos) {
      if(!(saleVins.includes(auto.vin))) {
        notSold.push(auto)
      }
    };

    return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form onSubmit={handleSubmit} id="create-sale-form">
                  <h1 className="card-title">Record a New Sale</h1>
                  <p className="mb-3">
                    Automobile VIN
                  </p>
                  <div className="mb-3">
                    <select onChange={handleSaleFormChange} value={formData.automobile} name="automobile" id="automobile" required>
                      <option value="automobile">Choose an Automobile VIN</option>
                      {
                        notSold.map(autos => {
                          return (
                            <option key={autos.id} value={autos.vin}>{autos.vin}</option>
                          );
                        })
                      }
                    </select>
                  </div>
                  <p className="mb-3">
                    Salesperson
                  </p>
                  <div className="mb-3">
                    <select onChange={handleSaleFormChange} value={formData.salesperson} name="salesperson" id="salesperson" required>
                      <option value="salesperson">Choose a Salesperson</option>
                      {
                        salesperson.map(salespeople=> {
                          return (
                            <option key={salespeople.id} value={salespeople.id}>{salespeople.first_name} {salespeople.last_name}</option>
                          )
                        })
                      }
                    </select>
                  </div>
                  <p className="mb-3">
                    Customer
                  </p>
                  <div className="mb-3">
                    <select onChange={handleSaleFormChange} value={formData.customer} name="customer" id="customer" required>
                      <option value="customer">Choose a Customer</option>
                      {
                        customers.map(customers=> {
                          return (
                            <option key={customers.id} value={customers.id}>{customers.first_name} {customers.last_name}</option>
                          )
                        })
                      }
                    </select>
                  </div>
                  <p className="mb-3">
                    Price
                  </p>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleSaleFormChange} value={formData.price} required placeholder="price" type="text" id="price" name="price" className="form-control" pattern="\$\d+\.\d{2}" />
                        <label htmlFor="price"> Required Format ex. = "$12000.00" </label>
                      </div>
                    </div>
                  <button className="btn btn-lg btn-primary">Add Sale</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default AddSaleForm;
