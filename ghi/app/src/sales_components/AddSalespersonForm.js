import React, {useState, useEffect} from 'react';

function AddSalespersonForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/salespeople/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        };
    };

    const handleAddSalespersonFormChange = (e) => {

        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form onSubmit={handleSubmit} id="create-salesperson-form">
                  <h1 className="card-title">Add a Salesperson</h1>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddSalespersonFormChange} value={formData.first_name} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" />
                        <label htmlFor="first_name">First Name</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddSalespersonFormChange} value={formData.last_name} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" />
                        <label htmlFor="last_name">Last Name</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddSalespersonFormChange} value={formData.employee_id} required placeholder="Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                        <label htmlFor="employee_id">Employee ID</label>
                        </div>
                      </div>
                    </div>
                  <button className="btn btn-lg btn-primary">Add</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default AddSalespersonForm;
