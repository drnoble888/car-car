import { useEffect, useState } from 'react';

function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        };
    };

    useEffect(()=>{
        getData();
    }, []);

    return (
        <div className="list-container">
        <h2 className="list-title">Salespeople</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {salespeople.sort((a,b) => a.id - b.id).map(salespeople => {
              return (
                <tr key={salespeople.id}>
                  <td> { salespeople.employee_id }</td>
                  <td>{ salespeople.first_name }</td>
                  <td>{ salespeople.last_name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    </div>
    );
};

export default SalespeopleList;
