import React, {useState, useEffect} from 'react';

function AddAutomobileForm() {
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });
    const [models, setModels] = useState([]);

    const getData = async () => {
        const modelsUrl = `http://localhost:8100/api/models/`;
        const modelsResponse =  await fetch(modelsUrl);

        if (modelsResponse.ok) {
            const modelsData = await modelsResponse.json();
            setModels(modelsData.models);
        };
    };

    useEffect(() => {
        getData();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/automobiles/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });

        };
    };

    const handleAddAutomobileFormChange = (e) => {

        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const uniqueModel = [];
    const uniqueModelId = [];

    for (let model of models){
      if(!(uniqueModelId.includes(model.id))) {
        uniqueModel.push(model);
        uniqueModelId.push(model.id);
      };
    };

    return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                  <h1 className="card-title">Add Automobile to Inventory</h1>
                  <div className="row">
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddAutomobileFormChange} value={formData.color} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddAutomobileFormChange} value={formData.year} required placeholder="Year" type="text" id="year" name="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-floating mb-3">
                        <input onChange={handleAddAutomobileFormChange} value={formData.vin} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                        <label htmlFor="vin">VIN</label>
                      </div>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleAddAutomobileFormChange} value={formData.model_id} name="model_id" id="model_id" required>
                      <option value="model_id">Choose a Model</option>
                      {
                        uniqueModel.map(model => {
                            return (
                            <option key={model.id} value={model.id}>{model.name}</option>
                            );
                        })
                      };
                    </select>
                  </div>
                  <button className="btn btn-lg btn-primary">Add</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default AddAutomobileForm;
