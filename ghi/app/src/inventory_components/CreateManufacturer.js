import React, {useState, useEffect} from 'react';

function CreateManufacturerForm() {
    const [formData, setFormData] = useState({
        name: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/manufacturers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        if (response.ok) {
            setFormData({
                name: '',
            });

        };
    };

    const handleCreateManufacturerFormChange = (e) => {

        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                  <h1 className="card-title">Create a Manufacturer</h1>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleCreateManufacturerFormChange} value={formData.name} required placeholder="Name" type="text" id="name" name="name" className="form-control" />
                        <label htmlFor="name">Manufacturer Name</label>
                      </div>
                    </div>
                    </div>
                  <button className="btn btn-lg btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default CreateManufacturerForm;
