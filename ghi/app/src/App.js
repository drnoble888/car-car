import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddSalespersonForm from './sales_components/AddSalespersonForm';
import SalespeopleList from './sales_components/SalespeopleList';
import AddCustomerForm from './sales_components/AddCustomerForm';
import CustomerList from './sales_components/CustomerList';
import AddSaleForm from './sales_components/AddSale';
import SalesList from './sales_components/SalesList';
import SalespersonHistory from './sales_components/SalespersonHistory';
import CreateManufacturerForm from './inventory_components/CreateManufacturer';
import ManufacturerList from './inventory_components/ManufacturerList';
import TechnicianList from './services_components/technicians';
import AppointmentList from './services_components/appointments';
import ServiceHistoryList from './services_components/service_history';
import AddTechnician from './services_components/create_technician';
import CreateAppointment from './services_components/create_appointment';
import AddVehicle from './inventory_components/create_vehicle';
import VehicleModelsList from './inventory_components/ListVehicleModels';
import AutomobilesList from './inventory_components/AutomobilesList';
import AddAutomobileForm from './inventory_components/AddAutomobile';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="create_manufacturer" element={<CreateManufacturerForm />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="add_vehicle" element={<AddVehicle />} />
          <Route path="add_salespeople" element={<AddSalespersonForm />} />
          <Route path="salespeople" element={<SalespeopleList />} />
          <Route path="add_customers" element={<AddCustomerForm />} />
          <Route path="customers" element={<CustomerList />} />
          <Route path="add_sales" element={<AddSaleForm />} />
          <Route path="sales" element={<SalesList />} />
          <Route path="salesperson_history" element={<SalespersonHistory />} />
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="add_technician" element={<AddTechnician />} />
          <Route path="appointments" element={<AppointmentList />} />
          <Route path="create_appointment" element={<CreateAppointment />} />
          <Route path="history" element={<ServiceHistoryList />} />
          <Route path="models" element={<VehicleModelsList />} />
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="add_automobiles" element={<AddAutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
