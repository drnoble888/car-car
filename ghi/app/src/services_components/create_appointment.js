import React, {useState, useEffect } from 'react';

function CreateAppointment() {
  const [autos, setAutos] = useState([]);
  const [techs, setTechs] = useState([]);
  const [formData, setFormData] = useState({
    vin: '',
    customer: '',
    date_time: '',
    technician:'',
    reason:'',
  });

  const getData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    };
  };

  useEffect(() => {
    getData();
  }, []);

  const getTech = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechs(data.technician);
    };
  };

  useEffect(() => {
    getTech();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = `http://localhost:8080/api/appointments/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: '',
        customer: '',
        date_time: '',
        technician:'',
        reason:'',
      });
    };
  };

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  return (
    <>
    <h1 className="my-3">Add an Appointment!</h1>
    <div className="my-3">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form onSubmit={handleSubmit} id="create-attendee-form">

                <div className="mb-3">
                <select onChange={handleChangeName} value={formData.technician} name="technician" id="technician" required>
                    <option value="technician">Choose a technician</option>
                    {
                    techs.map(tech => {
                        return (
                        <option key={tech.id} value={tech.id}>{tech.first_name} {tech.last_name}</option>
                        );
                    })
                    };
                </select>
                </div>
                <div className="col">
                    <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.vin} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                      <label htmlFor="vin">VIN</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.customer} required placeholder="Customer" type="text" id="customer" name="customer" className="form-control" />
                      <label htmlFor="customer">Customer</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.date_time} required placeholder="Date" type="date" id="date_time" name="date_time" className="form-control" />
                      <label htmlFor="date_time">Date</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.reason} required placeholder="Reason" type="text" id="reason" name="reason" className="form-control" />
                      <label htmlFor="reason">Reason</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default CreateAppointment;
