import React, {useState } from 'react';

function AddTechnician() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = `http://localhost:8080/api/technicians/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        employee_id: '',
      });
    };
  };

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  return (
    <>
    <h1 className="my-3">Add a Technician!</h1>
    <div className="my-3">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form onSubmit={handleSubmit} id="create-attendee-form">
                <div className="col">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.first_name} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="first_name">First Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.last_name} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="last_name">Last Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.employee_id} required placeholder="Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                      <label htmlFor="employee_id">Employee ID</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default AddTechnician;
