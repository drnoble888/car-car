from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Technician, AutomobileVO, Appointment
from .encoders import TechnicianListEncoder, AppointmentListEncoder

####################### GET/CREATE TECHNICIAN ###############################

@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        try:
            technician = Technician.objects.all()
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianListEncoder,
                )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response

####################### GET/CREATE APPOINTMENT ##############################

@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.all()
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentListEncoder,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            tech_id = content["technician"]
            tech_arr = Technician.objects.get(id=tech_id)
            content["technician"] = tech_arr
            appointment = Appointment.objects.create(**content)
            vin = content["vin"]
            try:
                automobile = AutomobileVO.objects.get(vin=vin)
                if vin == automobile.vin:
                    appointment.vip = True
                    appointment.save()
            except AutomobileVO.DoesNotExist:
                pass
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

####################### DELETE TECHNICIAN ###################################

@require_http_methods(["DELETE"])
def api_technician_detail(request, pk):
    try:
        tech = Technician.objects.get(id=pk)
        tech.delete()
        return JsonResponse(
            tech,
            encoder=TechnicianListEncoder,
            safe=False,
        )
    except Technician.DoesNotExist:
        response = JsonResponse({"message": "Technician does not exist"})
        response.status_code = 404
        return response

####################### DELETE APPOINTMENT ##################################

@require_http_methods(["DELETE"])
def api_appointment_detail(request, pk):
    try:
        bin = Appointment.objects.get(id=pk)
        bin.delete()
        return JsonResponse(
            bin,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response

####################### UPDATE APPOINTMENT TO CANCELED #######################

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response

####################### UPDATE APPOINTMENT TO FINSHED #######################

@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response
