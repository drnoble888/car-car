from django.urls import path
from .views import (api_technicians,
                    api_appointments,
                    api_technician_detail,
                    api_appointment_detail,
                    api_cancel_appointment,
                    api_finish_appointment)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("technicians/<int:pk>/", api_technician_detail, name="api_technician_detail"),
    path("appointments/<int:pk>/", api_appointment_detail, name="api_appointment_detail"),
    path("appointments/<int:pk>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<int:pk>/finish/", api_finish_appointment, name="api_finish_appointment")
]
